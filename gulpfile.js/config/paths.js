// var config = {};
// config.sourceDirectory = "./app";
// config.sourceAssets    = config.sourceDirectory + "/assets";
// var currentLang = '';
//
// function getConfig(lang) {
//   if( currentLang === '') {
//     currentLang = lang;
//   }
//   config.publicDirectory = "./public_" + currentLang;
//   config.publicAssets    = config.publicDirectory + "/assets";
// }
//
//
// module.exports = getConfig;

var config = {};

config.sourceDirectory = "./app";
config.publicDirectory = "./public";

config.sourceAssets    = config.sourceDirectory + "/assets";
config.publicAssets    = config.publicDirectory + "/assets";

module.exports = config;
