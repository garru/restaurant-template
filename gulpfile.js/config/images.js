var paths = require('./paths');

module.exports = {
  src: paths.sourceAssets + "/images/**",
  dest: paths.publicAssets + "/images"
};
