var paths = require('./paths');

module.exports = {
  autoprefixer: { browsers: ['last 2 version'] },

  src: paths.sourceAssets + "/stylesheets/block/**/*.less",

  vendor: paths.sourceAssets + "/stylesheets/libs/**/*.less",

  style: paths.sourceAssets + "/stylesheets/style.less",

  all: paths.sourceAssets + "/stylesheets/**/*.less",

  dest: paths.publicAssets + '/stylesheets',

  settings: {
    imagePath: 'assets/images' // Used by the image-url helper
  },

  cssLint: {
    "important": true,
    "adjoining-classes": false,
    "known-properties": false,
    "box-sizing": false,
    "box-model": true,
    "overqualified-elements": true,
    "display-property-grouping": true,
    "bulletproof-font-face": true,
    "compatible-vendor-prefixes": false,
    "regex-selectors": true,
    "errors": true,
    "duplicate-background-images": true,
    "duplicate-properties": true,
    "empty-rules": true,
    "selector-max-approaching": true,
    "gradients": true,
    "fallback-colors": true,
    "font-sizes": true,
    "font-faces": true,
    "floats": true,
    "star-property-hack": true,
    "outline-none": false,
    "import": true,
    "ids": true,
    "underscore-property-hack": true,
    "rules-count": true,
    "qualified-headings": false,
    "selector-max": true,
    "shorthand": true,
    "text-indent": true,
    "unique-headings": false,
    "universal-selector": true,
    "unqualified-attributes": true,
    "vendor-prefix": false,
    "zero-units": true
  }
};
