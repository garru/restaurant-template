var paths = require('./paths');

module.exports = {
  watch: paths.sourceDirectory + '/views/**/*.jade',
  src: [paths.sourceDirectory + '/views/**/*.jade', '!**/{_layout,_components,_includes}/**'],
  dest: paths.publicDirectory,
  settings: {
    pretty: true
  }
}
