var paths = require('./paths');

module.exports = {
  src: paths.publicDirectory + '/**/*',
  settings: {
    host: '',
    user: '',
    pass: '',
    remotePath: ''
  }
};
