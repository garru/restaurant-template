var paths = require('./paths');


module.exports = {
  // A separate bundle will be generated for each
  // bundle config in the list below
  bundleConfigs: [
    {
      entries: paths.sourceAssets + "/javascript/bundle.js",
      dest: paths.publicAssets + "/javascript",
      outputName: "bundle.js"
    }
  ]
};
