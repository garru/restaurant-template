var paths = require('./paths');

module.exports = {
  src: paths.sourceAssets + '/javascript/**/*',
  
  dest: paths.publicAssets + '/javascript',

  settings: {
    "curly": true,
    "eqeqeq": true,
    "latedef": true,
    "newcap": true,
    "noarg": true,
    "sub": true,
    "undef": true,
    "unused": true,
    "boss": true,
    "eqnull": true,
    "evil": true,
    "loopfunc": true,
    "browser": true,
    "devel": true,
    "expr": true,
    "quotmark": "single",
    "globals": {
      "Modernizr": true,
      "jQuery": true
    }
  }
};
