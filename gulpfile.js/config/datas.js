var paths = require('./paths');

module.exports = {
  src: paths.sourceAssets + '/datas/**/*',
  dest: paths.publicAssets + '/datas'
};
