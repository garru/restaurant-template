var paths = require('./paths');
var fontConfig = require('./fonts');

module.exports = {
  name: 'Gulp Starter Icons',
  src: paths.sourceAssets + '/icons/*.svg',
  dest: fontConfig.dest + '/icons',
  sassDest: paths.sourceAssets + '/stylesheets/base',
  template: './gulpfile.js/tasks/iconFont/template.scss',
  sassOutputName: '_icons.scss',
  fontPath: '../../assets/fonts/icons',
  className: 'icon',
  options: {
    fontName: 'icons',
    appendCodepoints: false,
    normalize: true,
    descent: 256,
    fontHeight: 1792,
    normalize: false,
    centerHorizontally: false,
    fixedWidth: false
  }
};
