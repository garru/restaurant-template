var paths = require('./paths');

module.exports = {
  server: {
    baseDir: paths.publicDirectory,
    routes: {
      "/app/assets/bower_components": paths.sourceAssets + "/bower_components"
    }
  },
  files: [paths.publicDirectory + '/**/*.html']
};
