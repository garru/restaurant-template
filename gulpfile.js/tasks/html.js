var gulp          = require('gulp');
var jade          = require('gulp-jade');
var gulpif        = require('gulp-if');
var uglify        = require('gulp-uglify');
var useref        = require('gulp-useref');
var lodash        = require('lodash');
var config        = require('../config/html');
var handleErrors  = require('../lib/handleErrors');


//___________________________________ functions
//

function jadeTask(lang) {

  if( lang !== undefined) {
    // Add locals option
    // _.extend(config, { locals: true });
  }

  return gulp.src(config.src)
    .pipe(jade(config.settings))
    .on('error', handleErrors)
    .pipe(gulp.dest(config.dest));
}

function useRefTask() {

  var assets = useref.assets();

  return gulp.src(config.dest + "/*.html")
    .pipe(assets)
    .pipe(gulpif('*.js', uglify()))
    .pipe(assets.restore())
    .pipe(useref())
    .pipe(gulp.dest(config.dest));
}



//___________________________________ tasks
//

gulp.task('jade', function() {
  return jadeTask();
});

gulp.task('html', ['jade'], function() {
  return useRefTask();
});
