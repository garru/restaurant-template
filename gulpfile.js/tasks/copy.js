var gulp          = require('gulp');
var gulpChanged   = require('gulp-changed');
var browserSync   = require('browser-sync');

var fonts         = require('../config/fonts');
var datas         = require('../config/datas');


//________________________________________________________ functions
// -

function copyTask(pConfig) {
  return gulp.src(pConfig.src)
    .pipe(gulpChanged(pConfig.dest))
    .pipe(gulp.dest(pConfig.dest))
    .pipe(browserSync.reload({stream:true}));
}

//________________________________________________________ tasks
// -

gulp.task('fonts', function() {
  return copyTask(fonts);
});

gulp.task('datas', function(){
  return copyTask(datas);
})

gulp.task('copy', ['fonts', 'datas']);
