var gulp      = require('gulp');
var concat    = require('gulp-concat');
var uglify    = require('gulp-uglify');
var rename    = require('gulp-rename');
var jshint    = require('gulp-jshint');
//var jslint    = require('gulp-jslint');
var config    = require('../config/paths');

//___________________________________ tasks
//

var jsSrc = {
  plugins: [config.sourceAssets + '/javascript/*.js', config.sourceAssets + '/javascript/plugins/*.js'],
  vendors: [
    config.sourceAssets + '/javascript/libs/*.js'
  ],
  modernizr: [
    config.sourceAssets + '/javascript/libs/modernizr.js',
    config.sourceAssets + '/javascript/libs/detectizr.js'
  ]
};

gulp.task('plugins', function() {
  return gulp.src(jsSrc.plugins)
    .pipe(concat('script.js'))
    .pipe(gulp.dest(config.publicAssets + '/javascript'));
});

gulp.task('vendors', function() {
  return gulp.src(jsSrc.vendors)
    .pipe(jshint(config.settings))
    .pipe(jshint.reporter('default'))
    .pipe(concat('libs.js'))
    .pipe(gulp.dest(config.publicAssets + '/javascript'));
});

gulp.task('modernizr', function() {
  return gulp.src(jsSrc.modernizr)
    .pipe(concat('modernizr.js'))
    .pipe(uglify())
    .pipe(gulp.dest(config.publicAssets + '/javascript'));
});

gulp.task('plugins:production', function() {
  return gulp.src(jsSrc.plugins)
    .pipe(concat('lib.js'))
    .pipe(jshint(config.settings))
    .pipe(jshint.reporter('default'))
    .pipe(uglify())
    .pipe(rename({
        suffix: '.min'
      }))
    .pipe(gulp.dest(config.publicAssets + '/javascript'));
});

gulp.task('uglifyJs', ['browserify', 'vendors', 'plugins','modernizr']);
gulp.task('uglifyJs:production', ['browserify', 'vendors', 'plugins:production']);
