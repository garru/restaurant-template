var gulp          = require('gulp');
var minify        = require('gulp-minify-css');
var sass          = require('gulp-sass');
var sourcemaps    = require('gulp-sourcemaps');
var autoprefixer  = require('gulp-autoprefixer');
var size          = require('gulp-filesize');
var csslint       = require('gulp-csslint');
var rename        = require('gulp-rename');
var concat        = require('gulp-concat');
var browserSync   = require('browser-sync');
var config        = require('../config/sass');
var handleErrors  = require('../lib/handleErrors');

var merge2        = require('merge2');

//___________________________________ functions
//

function buildSass(pIsProduction) {

  if( pIsProduction) {
    return gulp.src(config.all)
      .pipe(sourcemaps.init())
      .pipe(sass(config.settings))
      .on('error', handleErrors)
      .pipe(autoprefixer(config.autoprefixer))
      .pipe(minify())
      .pipe(rename('styles.css'))
      .pipe(gulp.dest(config.dest))
      .pipe(size());
  }

  else {
    var vendor = gulp.src(config.vendor)
      .pipe(sourcemaps.init())
      .pipe(sass(config.settings))
      .on('error', handleErrors)
      .pipe(autoprefixer(config.autoprefixer))
      
    var src = gulp.src(config.src)
      .pipe(sourcemaps.init())
      .pipe(sass(config.settings))
      .on('error', handleErrors)
      .pipe(csslint(config.cssLint))
      .pipe(csslint.reporter())
      .pipe(sourcemaps.write())
      .pipe(autoprefixer(config.autoprefixer))
      
    return merge2(vendor, src)
      .pipe(concat('styles.css'))
      .pipe(gulp.dest(config.dest))
      .pipe(browserSync.reload({stream:true}));
  }
}

function sassDebug() {
  return buildSass(false);
}

function sassProd() {
  return buildSass(true);
}

//___________________________________ tasks
//

gulp.task('sass', sassDebug);

gulp.task('sass:production', sassProd);
