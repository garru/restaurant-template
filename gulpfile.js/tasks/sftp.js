var gulp      = require('gulp');
var sftp  = require('gulp-sftp');
var config    = require('../config/sftp');

//___________________________________ functions
//

function uploadSftp() {
  return gulp.src(config.src)
    .pipe(sftp(config.settings));
}


//___________________________________ tasks
//

gulp.task('sftp', ['build:production'], uploadSftp);
