var gulp            = require('gulp');
var browserSync     = require('browser-sync');
var gulpChanged     = require('gulp-changed');
var gulpImagemin    = require('gulp-imagemin');
var config          = require('../config/images');

gulp.task('images', function() {
  return gulp.src(config.src)
    .pipe(gulpChanged(config.dest)) // Ignore unchanged files
    // .pipe(gulpImagemin()) // Optimize
    .pipe(gulp.dest(config.dest))
    .pipe(browserSync.reload({stream:true}));
});
