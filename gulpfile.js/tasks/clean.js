var gulp            = require('gulp');
var del             = require('del');
var configPath      = require('../config/paths');
var iconFontConfig  = require('../config/iconFont');


//___________________________________ functions
//

function clean(pTarget, cb) {
  del(pTarget, cb);
}


//___________________________________ tasks
//

gulp.task('clean', clean.bind(null, [
  configPath.publicDirectory,
  iconFontConfig.sassDest + '/_icons.scss'
]));

gulp.task('clean-modules', clean.bind(null, [
  'node_modules'
]));
