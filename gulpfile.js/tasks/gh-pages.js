var gulp      = require('gulp-param')(require('gulp'), process.argv);
var ghPages   = require('gulp-gh-pages');
var config    = require('../config/sftp');


//___________________________________ functions
//

function ghPagesTask(m) {
  var configGh = {};
  if( m !== null)
    configGh.message = m;

  return gulp.src(config.src)
    .pipe(ghPages(configGh));
}

//___________________________________ tasks
//

gulp.task('ghPages', ['build:production'], ghPagesTask);
