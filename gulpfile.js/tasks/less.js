var gulp          = require('gulp');
var minify        = require('gulp-minify-css');
var less          = require('gulp-less');
var sourcemaps    = require('gulp-sourcemaps');
var autoprefixer  = require('gulp-autoprefixer');
var size          = require('gulp-filesize');
var csslint       = require('gulp-csslint');
var rename        = require('gulp-rename');
var concat        = require('gulp-concat');
var browserSync   = require('browser-sync');
var config        = require('../config/less');
var handleErrors  = require('../lib/handleErrors');

var merge2        = require('merge2');

//___________________________________ functions
//

function buildLess(pIsProduction) {

  if( pIsProduction) {
    return gulp.src(config.all)
      //.pipe(sourcemaps.init())
      .pipe(less())
      .on('error', handleErrors)
      .pipe(autoprefixer(config.autoprefixer))
      .pipe(minify())
      .pipe(rename('styles.css'))
      .pipe(gulp.dest(config.dest))
      .pipe(size());
  }

  else {
    // var vendor = gulp.src(config.vendor)
    //   //.pipe(sourcemaps.init())
    //    .pipe(less())
    //   .on('error', handleErrors)
    //   .pipe(autoprefixer(config.autoprefixer))
      
    // var src = gulp.src(config.src)
    //   //.pipe(sourcemaps.init())
    //   .pipe(less())
    //   .on('error', handleErrors)
    //   .pipe(csslint(config.cssLint))
    //   .pipe(csslint.reporter())
    //   //.pipe(sourcemaps.write())
    //   .pipe(autoprefixer(config.autoprefixer))
    
    var style = gulp.src(config.style)
      .pipe(less())
      .on('error', handleErrors)
      .pipe(csslint(config.cssLint))
      .pipe(csslint.reporter())
      //.pipe(sourcemaps.write())
      .pipe(autoprefixer(config.autoprefixer))
      
    return merge2(style)
      .pipe(concat('styles.css'))
      .pipe(gulp.dest(config.dest))
      .pipe(browserSync.reload({stream:true}));
  }
}

function lessDebug() {
  return buildLess(false);
}

function lessProd() {
  return buildLess(true);
}

//___________________________________ tasks
//

gulp.task('less', lessDebug);

gulp.task('less:production', lessProd);
