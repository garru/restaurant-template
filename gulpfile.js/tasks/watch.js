var gulp      = require('gulp');
var watch     = require('gulp-watch');

var images    = require('../config/images');
var fonts     = require('../config/fonts');
var iconFont  = require('../config/iconFont');
var sass      = require('../config/sass');
var less      = require('../config/less');
var html      = require('../config/html');
var datas     = require('../config/datas');
var js        = require('../config/javascript');

gulp.task('watch', ['browserSync'], function() {
  watch(images.src,   function() { gulp.start('images');    });
  watch(fonts.src,    function() { gulp.start('fonts');     });
  watch(datas.src,    function() { gulp.start('datas');     });
  watch(iconFont.src, function() { gulp.start('iconFont');  });
  watch(js.src,     function() { gulp.start('uglifyJs');      });
  watch(sass.all,     function() { gulp.start('sass');      });
  watch(less.all,     function() { gulp.start('less');      });
  watch(html.watch,   function() { gulp.start('jade');      });
});
