var gulp          = require('gulp');
var gulpSequence  = require('gulp-sequence');


//___________________________________ tasks
//

gulp.task('build:development', gulpSequence('clean', ['copy', 'iconFont', 'images'], ['uglifyJs', 'less', 'watchify', 'jade'], ['watch']));

gulp.task('build:production', function(cb) {
  process.env.NODE_ENV = 'production';
  gulpSequence('clean', ['copy', 'iconFont', 'images'], ['uglifyJs:production', 'sass:production', 'browserify', 'html'], cb);
});
