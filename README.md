# PROJECT

## Install dependencies
```
npm install
bower install
```

This runs through all dependencies listed in `package.json` and `bower.json` and downloads them to a `node_modules` and `app/bower_components` folders in your project directory.

## Run gulp
The first time you run the app, you'll also need to generate the iconFont, since this is not something we want to run every time with our `default` task.
```
gulp iconFont
```

After that, just run the `default` gulp task with:
```
gulp
```

## Configuration
All paths and plugin settings have been abstracted into a centralized config folder in `gulp/config/`.

## Additional Features and Tasks

### Icon Fonts
```
gulp iconFont
```
Generating and re-generating icon fonts is an every once and a while task, so it's not included in `tasks/default.js`. Run the task separately any time you add an svg to your icons folder. This task has a couple of parts.

### Tasks

```
gulp build:develop [options]
```
The default task. Compile styles, javascripts, jade files and start watcher
* --lang : localisation

```
gulp build:production [options]
```
The production task. Compile styles and javascripts, then uglify, minify, optimize images and compile fonts.
* --lang : localisation


```
gulp ghPages [options]
```
Push current branch under the name gh-pages to the repository width specified comment in parameter.
* --m : comment
* --lang : localisation


```
gulp deploy-sftp [options]
```
Deploy current branch on creativetechnology
* --lang : localisation


```
gulp clean
```
Delete public folder and generated css from `iconFont` task
