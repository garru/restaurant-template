(function($) {
  'use strict';

  var pluginName = 'header',
      win = $(window);

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
    this.toggleSubMenu();
  }
  Plugin.prototype = {
    init: function() {
      var that = this,
          el = that.element,
          headerMenu = el.find('[data-menu]'),
          headerHeight = 20,
          posHeaderSCroll = 80;
      win.on('scroll.' + pluginName, function(){
        if(win.scrollTop() > posHeaderSCroll - headerHeight) {
          headerMenu.css('background-color','black');
        } else {
          headerMenu.css('background-color','rgba(0, 0, 0, 0)');
        }
      }).trigger('scroll.' + pluginName);
    },

    toggleSubMenu: function() {
      var that = this,
          el = that.element,
          menu = el.find('[data-menu]'),
          itemHasSub = menu.find('.menu-item-has-children'),
          subMenu = menu.find('[data-sub-menu]'),
          click = 'click.' + pluginName,
          activeClass = 'open',
          event,
          timer,
          duration = 300,
          activeIndex;
      Site.isDesktop() ? event = 'resize.' : event = 'orientationchange.';
      Site.win.on(event + pluginName, function() {
        clearTimeout(timer);
        timer = setTimeout(function() {
          if(Site.isMobile()) {
            itemHasSub.each(function(idx) {
              var $that = $(this),
                  height = $that.children('a').innerHeight();
              if(idx === activeClass) {
                var heightSubMenu = $that.children('[data-sub-menu]').innerHeight();
                $that.height(height + heightSubMenu);
              }
              else {
                $that.height(height);
              }
            });
            itemHasSub.off(click).on(click, function() {
              var that = $(this),
                  idx = itemHasSub.index(that),
                  height,
                  heightTemp;
              if (idx === activeIndex) {
                height = itemHasSub.eq(idx).children('a').innerHeight();
                that.height(height);
                subMenu.eq(idx).removeClass(activeClass);
                that.removeClass(activeClass);
                activeIndex = null;
              }
              else {
                height = itemHasSub.eq(idx).children('a').innerHeight() + subMenu.eq(idx).innerHeight();
                that.height(height);
                subMenu.eq(idx).addClass(activeClass);
                that.addClass(activeClass);
                if (activeIndex !== null) {
                  heightTemp = itemHasSub.eq(activeIndex).children('a').innerHeight();
                  itemHasSub.eq(activeIndex).height(heightTemp);
                  subMenu.eq(activeIndex).removeClass(activeClass);
                  itemHasSub.eq(activeIndex).removeClass(activeClass);
                }
                activeIndex = idx;
              }
            });
          }
          else {
            itemHasSub.height('');
            subMenu.height('');
            itemHasSub.removeClass(activeClass);
            subMenu.removeClass(activeClass);
          }
        }, duration);
      }).trigger(event + pluginName);
    }

  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      }
    });
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]();
  });
})(jQuery);
