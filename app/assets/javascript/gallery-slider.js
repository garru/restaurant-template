(function ($) {
	'use strict';

	var pluginName = 'gallery-slider';

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
		this.init();
	}
	Plugin.prototype = {
		init: function () {
			this.initSliderNav();
		},

		initSliderNav: function() {
			var that = this,
				el = that.element,
				navSlider = el.find('[data-slider-nav]'),
				slider = el.find('[data-slider-primary]'),
				navId = navSlider.data('nav-id'),
				sliderId = slider.data('pri-nav-id'),
				optionNav = {
					arrows: true,
					centerMode: true,
					centerPadding: '20%',
					variableWidth: true,
					focusOnSelect: true,
					nextArrow: '<span class="slick-next fa fa-angle-right"></span>',
					prevArrow:'<span class="slick-prev fa fa-angle-left"></span>',
					speed: 500,
					asNavFor: navId,
					responsive: [
				    {
				      breakpoint: 1023,
				      settings: {
								slidesToShow: 1,
								slidesToScroll: 1,
								centerMode: true,
								centerPadding: '35px',
								arrows: false
				      }
				    }
				  ]
				},
				option = {
					arrows: false,
					centerMode: true,
					centerPadding: '0',
					slidesToShow: 1,
					asNavFor: sliderId,
					focusOnSelect: true,
					speed: 500,
					speed: 500,
				  fade: true,
				  cssEase: 'linear',
					responsive: [
				    {
				      breakpoint: 1023,
				      settings: {
								slidesToShow: 1,
								slidesToScroll: 1,
								centerMode: true,
								centerPadding: '35px',
								arrows: false
				      }
				    }
				  ]
				};
				navSlider.slick(optionNav);
				slider.slick(option);
		},
  };

	$.fn[pluginName] = function (options, params) {
		return this.each(function () {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			}
		});
	};

	$(function () {
		$('[data-' + pluginName + ']')[pluginName]();
	});
})(jQuery);
