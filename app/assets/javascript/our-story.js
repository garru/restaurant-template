;(function($, window, undefined) {
  'use strict';

  var pluginName = 'our-story',
    currentItem,
    curentIndex,
    lengthItem;
  

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }

  Plugin.prototype = {
    init: function() {
      this.initSlider();
      this.initSliderNav();
    },

    initSlider: function() {
      var that = this,
        el = this.element,
        slider = el.find('[data-slider]'),
        opt = that.options,
        idAsNav = slider.data('as-nav'),
        slidesToShow = opt.itemToShow,
        eventName,
        option,
        timer,
        isReset = true,
        intinialIndex;

      lengthItem = slider.children().length;
      currentItem = slidesToShow - 1;
      curentIndex = currentItem - 1;
      intinialIndex = lengthItem - curentIndex;
      Site.isDesktop() ? eventName = 'resize.' : eventName = 'orientationchange.';

      Site.win.on(eventName + pluginName, function() {
        clearTimeout(timer);
        timer = setTimeout(function(){

          if (Site.isMobile()) {
            option = {
              arrows: false,
              infinite: true,
              autoplay: false,
              slidesToShow: 1,
              variableWidth: false,
              initialSlide: 0,
              asNavFor: idAsNav,
              speed: 250
            };
            if (slider.hasClass('slick-slider')) {
              slider.slick('unslick');
            }
            slider.slick(option);
            if (!isReset) {
              that.resetSize();
              isReset = true;
            }
          } else {
            option = {
              arrows: false,
              infinite: true,
              autoplay: false,
              slidesToShow: slidesToShow,
              accessibility: false,
              variableWidth: true,
              draggable: false,
              initialSlide: 0,
              speed: 250
            };
            if (slider.hasClass('slick-slider')) {
              slider.slick('unslick');
            }
            slider.slick(option);
            that.calHeight();
            isReset = false;
          }
        }, 500);
      }).trigger(eventName + pluginName);
    },

    calHeight: function() {
      var that = this,
        el = that.element,
        opt = that.options,
        slider = el.find('[data-slider]'),
        slickClass = opt.slickClass,
        scaleRatio = opt.scaleRatio,
        itemToShow = opt.itemToShow,
        slickActiveClass = opt.slickActiveClass,
        widthEle = slider.width(),
        item,
        widthItem,
        itemPadding = 20,
        imgItem,
        widthImg,
        heightImg,
        originWidthImg,
        originHeightImg,
        activeItem,
        widthItemActive,
        heightItemActive,
        indexActiveItem = itemToShow - 2;

      item = slider.find(slickClass);
      imgItem = item.children();
      originWidthImg = imgItem.width();
      originHeightImg = imgItem.height();
      widthImg = (widthEle - itemToShow*itemPadding)/((itemToShow - 1) + scaleRatio); //show 4 normal image + 1 active image with width scaleRatio 
      heightImg = widthImg/originWidthImg*originHeightImg; // calculator Height Image when set new Width
      heightItemActive = heightImg*scaleRatio;
      widthItem = widthImg + itemPadding;
      widthItemActive = widthImg*scaleRatio + itemPadding;
      item.height(heightItemActive);
      item.outerWidth(widthItem);
      activeItem = slider.find(slickActiveClass).eq(indexActiveItem);
      activeItem.outerWidth(widthItemActive);

      slider.off('afterChange.' + pluginName).on('afterChange.' + pluginName, function(){
        activeItem.outerWidth(widthItem);
        activeItem = slider.find(slickActiveClass).eq(indexActiveItem);
        activeItem.outerWidth(widthItemActive);
      });
    },

    resetSize: function() {
      var that = this,
        el = that.element,
        opt = that.options,
        slickClass = opt.slickClass,
        slider = el.find('data-slider'),
        item = slider.find(slickClass);

      item.css({
        'width': '',
        'height': ''
      });
    },

    initSliderNav: function() {
      var that = this,
        el = this.element,
        opt = that.options,
        sliderNav = el.find('[data-slider-nav]'),
        slider = el.find('[data-slider]'),
        click = 'click.' + pluginName,
        hiddenClass = 'hidden',
        nextBtn,
        prevBtn,
        timer,
        eventName,
        idAsNav = sliderNav.data('as-nav'),
        slidesToShow = opt.itemToShow,
        option;

      Site.isDesktop() ? eventName = 'resize.' : eventName = 'orientationchange.';
      Site.win.on(eventName + pluginName, function() {
        clearTimeout(timer);
        timer = setTimeout(function(){
          if (Site.isMobile()) {
            option = {
              arrows: true,
              autoplay: false,
              asNavFor: idAsNav,
              fade: true,
              draggable: false,
              initialSlide: 0,
              accessibility: false,
              nextArrow: '<span class="slick-next splatter-ico-triagle-right"></span>',
              prevArrow:'<span class="slick-prev splatter-ico-triagle-right"></span>',
              speed: 250
            };
            if (sliderNav.hasClass('slick-slider')) {
              sliderNav.slick('unslick');
            }
            sliderNav.slick(option);
            nextBtn = sliderNav.find('.slick-next');
            prevBtn = sliderNav.find('.slick-prev');
            prevBtn.off(click).removeClass(hiddenClass);
            nextBtn.off(click).removeClass(hiddenClass);
          } else {
            option = {
              arrows: true,
              autoplay: false,
              fade: true,
              draggable: false,
              initialSlide: 3,
              accessibility: false,
              nextArrow: '<span class="slick-next splatter-ico-triagle-right"></span>',
              prevArrow:'<span class="slick-prev splatter-ico-triagle-right"></span>',
              speed: 250
            };
            if (sliderNav.hasClass('slick-slider')) {
              sliderNav.slick('unslick');
            }
            sliderNav.slick(option);
            currentItem = 0;
            nextBtn = sliderNav.find('.slick-next');
            prevBtn = sliderNav.find('.slick-prev');
            prevBtn.addClass(hiddenClass);
            nextBtn = sliderNav.find('.slick-next');
            prevBtn = sliderNav.find('.slick-prev');
            nextBtn.off(click).on(click, function() {
              slider.slick('slickNext');
              currentItem = currentItem + 1;
              if (currentItem === (lengthItem - 1)) {
                nextBtn.addClass(hiddenClass);
              } else if(prevBtn.hasClass(hiddenClass)) {
                prevBtn.removeClass(hiddenClass);
              }
            });
      
            prevBtn.off(click).on(click, function() {
              currentItem = currentItem - 1;
              slider.slick('slickPrev');
              if (currentItem === 0) {
                prevBtn.addClass(hiddenClass);
              } else if(nextBtn.hasClass(hiddenClass)) {
                nextBtn.removeClass(hiddenClass);
              }
            });
          }
        }, 500);
      }).trigger(eventName + pluginName);
    },
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      }
    });
  };

  $.fn[pluginName].defaults = {
    key: 'value',
    itemToShow: 5,
    scaleRatio: 1.68,
    slickClass : '.slick-slide',
    slickActiveClass : '.slick-active',
    onCallback: null
  };

  $(function() {
    $('[data-' + pluginName + ']').on('customEvent', function() {
      // to do
    });

    $('[data-' + pluginName + ']')[pluginName]({
      key: 'custom'
    });
  });

}(jQuery, window));