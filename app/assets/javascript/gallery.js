(function ($) {
	'use strict';

	var pluginName = 'gallery',
		activeClass = 'active',
		effectClass;

	function Plugin(element, options) {
		this.element = $(element);
		this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
		this.init();
	}
	Plugin.prototype = {
		init: function () {
			this.initClass();
			this.initSliderNav();
		},

		initClass: function() {
			var that = this,
				el = that.element,
				showBox = el.find('[data-show-box]'),
				image = showBox.children('img'),
				maxEffectIndex = 5, // 5 effect from 0 -> 4
				flipClass = 'flip',
				rotaClass = 'rota',
				zoomBotClass = 'zoom-bot',
				zoomTopClass = 'zoom-top',
				fadeClass = 'fade',
				count;
			image.eq(0).addClass(activeClass);
			image.each(function() {
				var _this = $(this);
				count = Math.floor(Math.random() * maxEffectIndex);
				switch(count) {
					case 0:
						_this.addClass(flipClass);
						break;
					case 1:
						_this.addClass(rotaClass);
						break;
					case 2:
						_this.addClass(zoomTopClass);
						break;
					case 3:
						_this.addClass(zoomBotClass);
						break;
					case 4:
						_this.addClass(zoomBotClass);
						break;
				}
			});
		},

		initSliderNav: function() {
			var that = this,
				el = that.element,
				showBox = el.find('[data-show-box]'),
				showBoxItem = showBox.children('img'),
				slider = el.find('[data-slider]'),
				indexActive = 0,
				option = {
					arrows: false,
					centerMode: true,
					autoplay: true,
					slidesToShow: 5,
					slidesToScroll: 1,
					focusOnSelect: true,
					initialSlide: 0,
					speed: 250,
					autoplaySpeed: 5000,
					responsive: [
				    {
				      breakpoint: 768,
				      settings: {
				        slidesToShow: 3,
				        slidesToScroll: 1
				      }
				    },
				    {
				      breakpoint: 480,
				      settings: {
				        slidesToShow: 2,
				        slidesToScroll: 1
				      }
				    }
				  ]
				};
			slider.slick(option);
			slider.off('afterChange.' + pluginName).on('afterChange.' + pluginName, function() {
				var activeItem = slider.find('.slick-current'),
					tempIndex = activeItem.data('img-index');
				showBoxItem.eq(indexActive).removeClass(activeClass);
				showBoxItem.eq(tempIndex).addClass(activeClass);
				indexActive = tempIndex;
			});
		},
  };

	$.fn[pluginName] = function (options, params) {
		return this.each(function () {
			var instance = $.data(this, pluginName);
			if (!instance) {
				$.data(this, pluginName, new Plugin(this, options));
			} else if (instance[options]) {
				instance[options](params);
			}
		});
	};

	$(function () {
		$('[data-' + pluginName + ']')[pluginName]();
	});
})(jQuery);
