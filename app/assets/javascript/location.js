(function($) {
    'use strict';

    var pluginName = 'location';

    function Plugin(element, options) {
      this.element = $(element);
      this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
      this.init();
    }
    Plugin.prototype = {
      init: function() {
        this.createMap();
      },
      createMap: function() {
        var that = this,
          el = that.element,
          activeClass = 'active',
          iframeBlock;

        iframeBlock = el.find('iframe');
        el.off('click.' + pluginName).on('click.' + pluginName, function(){
          iframeBlock.addClass(activeClass);
        });
        el.mouseleave(function(){
          iframeBlock.removeClass(activeClass);
        });
      }
    };

    $.fn[pluginName] = function(options, params) {
      return this.each(function() {
        var instance = $.data(this, pluginName);
        if (!instance) {
          $.data(this, pluginName, new Plugin(this, options));
        } else if (instance[options]) {
          instance[options](params);
        }
      });
    };

    $(function() {
      $('[data-' + pluginName + ']')[pluginName]();
    });
  })(jQuery);
