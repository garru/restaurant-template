//Global function here
window.Site = (function($, window, undefined) {
  var ajaxTemplate = [],
    pagingConfig = {
      total: 100, //Count of all item
      currentPage: 1, //Current page
      maxPerPage: 8, //How many item display per page
      visiblePages: 5 //Maximum page can display at once
    },
    win = $(window),
    html = $('html'),
    body = $('body'),
    googleApi = {
      clientId: '767753723332-d9jbsrcj0fqgbidgt2iunapf0r859k02.apps.googleusercontent.com',
      apiKey: 'YFOZDiS7PBS8RWnxi_kUFQBZ'
    };

  //Overlay for ajax
  var loadingOverLay = $('<div class="loading-block"><div class="loading-block__loader"></div></div>').appendTo('body');

  function isMobile() {
    return window.Modernizr.mq('(max-width: 1023px)');
  }

  function isDesktop() {
    return html.hasClass('desktop');
  }

  function isTouch() {
    return html.hasClass('touch');
  }

  function isAndroid() {
    return html.hasClass('android');
  }

  function isIOs() {
    return html.hasClass('ios');
  }

  function isTablet() {
    return html.hasClass('tablet');
  }

  function showLoader(show) {
    var defer = $.Deferred();
    if (show ) {
      loadingOverLay.finish().fadeIn(function(){
        defer.resolve();
      });
    } else {
      loadingOverLay.finish().fadeOut(function(){
        defer.resolve();
      });
    }
    return defer.promise();
  }

  function getDevice() {
    if (isMobile()) {
      return 'mobile';
    } else if (isDesktop()) {
      return 'desktop';
    }
  }

 
return {
  isMobile: isMobile,
  isDesktop: isDesktop,
  isTouch: isTouch,
  isAndroid: isAndroid,
  isIOs: isIOs,
  isTablet: isTablet,
  getDevice: getDevice,
  win: win,
  html: html,
  body: body
};

})(jQuery, window);
