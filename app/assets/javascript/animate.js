(function($) {
  'use strict';

  var pluginName = 'animate',
      activeClass = 'active',
      win = $(window);

  function Plugin(element, options) {
    this.element = $(element);
    this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
    this.init();
  }
  Plugin.prototype = {
    init: function() {
      var that = this,
        el = that.element,
        itemAnimate = el.find('[data-animate-item]'),
        scrollToShowItem = 100,
        eventName,
        windowHeight,
        timerResize;

      windowHeight = win.outerHeight(true);
      Site.isDesktop() ? eventName = 'resize.' : eventName = 'orientationchange.';
      win.on(eventName + pluginName, function(){
        clearTimeout(timerResize);
        timerResize = setTimeout(function(){
          itemAnimate.each(function(){
            var _this = $(this);
            if (_this.hasClass(activeClass)) {
              _this.removeClass(activeClass);
            }
          });
          windowHeight = win.outerHeight(true);
        }, 500);
      });

      win.on('scroll.' + pluginName, function(){
        var windowOffsetY = window.pageYOffset;
        itemAnimate.each(function(){
          var _this = $(this),
          topItem = _this.offset().top;
          if (windowOffsetY + windowHeight  > topItem + scrollToShowItem) {    // show animate when scroll to 80px of item
            if(!_this.hasClass(activeClass)) {
              _this.addClass(activeClass);
            }
          }
        });
      }).trigger('scroll.' + pluginName);
    },
  };

  $.fn[pluginName] = function(options, params) {
    return this.each(function() {
      var instance = $.data(this, pluginName);
      if (!instance) {
        $.data(this, pluginName, new Plugin(this, options));
      } else if (instance[options]) {
        instance[options](params);
      }
    });
  };

  $(function() {
    $('[data-' + pluginName + ']')[pluginName]();
  });
})(jQuery);
