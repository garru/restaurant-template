(function($) {
    'use strict';

    var pluginName = 'banner-home';

    function Plugin(element, options) {
      this.element = $(element);
      this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
      this.init();
    }
    Plugin.prototype = {
      init: function() {
        this.initSlider();
      },
      initSlider: function () {
        var that = this,
        el = that.element,
        activeClass = 'active',
        activeIndex = 0,
        activeItem,
        sliderItem,
        option,
        event,
        timer;
        
        Site.isDesktop() ? event = 'resize.' : event = 'orientationchange.';
        Site.win.on(event + pluginName, function() {
            clearTimeout(timer);
            timer = setTimeout(function(){
                option = {
                    arrows: false,
                    autoplay: true,
                    infinite : true,
                    slidesToShow: 1,
                    fade: true,
                    initialSlide: activeIndex,
                    autoplaySpeed: 5000,
                    speed: 1000
                };
                if (el.hasClass('slick-slider')) {
                    el.slick('unslick');
                }
                el.slick(option);
                sliderItem = el.find('[data-item]');
                activeItem = sliderItem.eq(activeIndex);
                if (!activeItem.hasClass(activeClass)) {
                    sliderItem.eq(activeIndex).addClass(activeClass);
                }
                el.off('afterChange.' + pluginName).on('afterChange.' + pluginName, function(){
                    sliderItem.eq(activeIndex).removeClass(activeClass);
                    activeItem = el.find('.slick-current');
                    activeItem.addClass(activeClass);
                    activeIndex = activeItem.data('slick-index');
                });
            }, 500);
        }).trigger(event + pluginName);
      },

      destroy: function() {
        $.removeData(this.element[0], pluginName);
      }
    };

    $.fn[pluginName] = function(options, params) {
      return this.each(function() {
        var instance = $.data(this, pluginName);
        if (!instance) {
          $.data(this, pluginName, new Plugin(this, options));
        } else if (instance[options]) {
          instance[options](params);
        }
      });
    };

    $(function() {
      $('[data-' + pluginName + ']')[pluginName]();
    });
  })(jQuery);
