(function($) {
    'use strict';

    var pluginName = 'testimonials-slider',
        win = $(window),
        eventName;

    function isDesktop() {
      return (win.width()>=1024);
    }
    function Plugin(element, options) {
      this.element = $(element);
      this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
      this.init();
    }
    Plugin.prototype = {
      init: function() {
        this.banner();
        this.button();
      },

      banner : function() {
        var el = this.element,
            slider = el.find('[data-slider]'),
            options = this.options;
        console.log(options);
        slider.slick(options);
      },

      button : function(){
        var el = this.element,
          btnNext = el.find('[data-btn-next]'),
          btnPrev = el.find('[data-btn-prev]'),
          slider = el.find('[data-slider]');
        btnNext.off('click').on('click', function(){
          slider.slick('slickNext');
        });
        btnPrev.off('click').on('click', function(){
          slider.slick('slickPrev');
        });
      },

      destroy: function() {
        $.removeData(this.element[0], pluginName);
      }
    };

    $.fn[pluginName] = function(options, params) {
      return this.each(function() {
        var instance = $.data(this, pluginName);
        if (!instance) {
          $.data(this, pluginName, new Plugin(this, options));
        } else if (instance[options]) {
          instance[options](params);
        }
      });
    };

    $(function() {
      $('[data-' + pluginName + ']')[pluginName]();
    });
  })(jQuery);
