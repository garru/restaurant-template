(function($) {
    'use strict';

    var pluginName = 'slider1',
        win = $(window),
        eventName;

    function isDesktop() {
      return (win.width()>=1024);
    }
    function Plugin(element, options) {
      this.element = $(element);
      this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
      this.init();
    }
    Plugin.prototype = {
      init: function() {
        this.banner();

      },
      banner : function(){
        var el = this.element,
            options = this.options;
        el.slick(options);
      },

      destroy: function() {
        $.removeData(this.element[0], pluginName);
      }
    };

    $.fn[pluginName] = function(options, params) {
      return this.each(function() {
        var instance = $.data(this, pluginName);
        if (!instance) {
          $.data(this, pluginName, new Plugin(this, options));
        } else if (instance[options]) {
          instance[options](params);
        }
      });
    };
    $.fn[pluginName].defaults = {
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      autoplay: false,
      duration: 300
    }

    $(function() {
      $('[data-' + pluginName + ']')[pluginName]();
    });
  })(jQuery);
