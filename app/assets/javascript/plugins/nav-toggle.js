(function($) {
    'use strict';

    var pluginName = 'nav-toggle',
    body = $('body'),
    header = body.find('[data-header]');

    function Plugin(element, options) {
      this.element = $(element);
      this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
      this.init();
    }
    Plugin.prototype = {
      init: function() {
        this.btnToggle();
      },
      btnToggle : function() {
        var el = this.element,
            menu = $(el.attr('data-nav-toggle')),
            posMenu = header.find('[data-top]').outerHeight(),
            duration = 500,
            event,
            timer,
            frozen = 'frozen';
        Site.isDesktop() ? event = 'resize.' : event = 'orientationchange.';
        Site.win.on(event + pluginName, function() {
          clearTimeout(timer);
          timer = setTimeout(function() {
            if(Site.isMobile()) {
              menu.css('padding-top', posMenu);
              menu.css('padding-bottom', posMenu);
              el.off('click').on('click', function() {
                if(el.hasClass('open')) {
                  el.removeClass('open');
                  menu.removeClass('open');
                  header.removeClass('open-menu');
                  body.removeClass(frozen);
                }
                else {
                  el.addClass('open');
                  menu.addClass('open');
                  header.addClass('open-menu');
                  body.addClass(frozen);
                }
              });
            }
            else {
              menu.css('padding-top', '');
              menu.css('padding-bottom', '');
              body.removeClass(frozen);
              el.removeClass('open');
              menu.removeClass('open');
              header.removeClass('open-menu');
            }
          }, duration);
        }).trigger(event + pluginName);
      },

      destroy: function() {
        $.removeData(this.element[0], pluginName);
      }
    };

    $.fn[pluginName] = function(options, params) {
      return this.each(function() {
        var instance = $.data(this, pluginName);
        if (!instance) {
          $.data(this, pluginName, new Plugin(this, options));
        } else if (instance[options]) {
          instance[options](params);
        }
      });
    };

    $(function() {
      $('[data-' + pluginName + ']')[pluginName]();
    });
  })(jQuery);
