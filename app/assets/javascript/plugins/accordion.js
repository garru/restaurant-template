(function($) {
    'use strict';

    var pluginName = 'accordion',
    activeIndex;

    function Plugin(element, options) {
      this.element = $(element);
      this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
      this.init();
    }
    Plugin.prototype = {
      init: function() {
        this.toggle();
      },

      toggle: function() {
        var that = this,
          el = that.element,
          item = el.find('[data-item]'),
          title = el.find('[data-title]'),
          panel = el.find('[data-panel]'),
          click = 'click.' + pluginName,
          activeClass = 'active',
          event,
          timer;

        Site.isDesktop() ? event = 'resize.' : event = 'orientationchange.';
        Site.win.on(event + pluginName, function() {
          clearTimeout(timer);
          timer = setTimeout(function(){
            if(Site.isMobile()) {
              item.each(function(idx){
                var $this = $(this),
                  title = $this.find('[data-title]'),
                  height = title.outerHeight(true);
                title.css('cursor', '');
                if (idx === activeIndex) {
                  var heightPanel = $this.find('[data-panel]').outerHeight(true);
                  $this.height(height + heightPanel);
                } else {
                  $this.height(height);
                }
              });

              title.off(click).on(click, function() {
                var $this = $(this),
                  idx = title.index($this),
                  height,
                  heightTemp;
      
                if (idx === activeIndex) {
                  height = title.eq(idx).outerHeight(true);
                  item.eq(idx).height(height);
                  $this.removeClass(activeClass);
                  activeIndex = null;
                } else {
                  height = title.eq(idx).outerHeight(true) + panel.eq(idx).outerHeight(true);
                  $this.addClass(activeClass);
                  item.eq(idx).height(height);
                  if (activeIndex !== null) {
                    heightTemp = title.eq(activeIndex).outerHeight(true);
                    title.eq(activeIndex).removeClass(activeClass);
                    item.eq(activeIndex).height(heightTemp);
                  }
                  activeIndex = idx;
                }
              });

            } else {
              item.off(click);
              item.each(function(idx){
                $(this).css('height', '');
                $(this).find('[data-title]').css('cursor', 'default');
              });
            }
          }, 500);
        }).trigger(event + pluginName);
      },

      destroy: function() {
        $.removeData(this.element[0], pluginName);
      }
    };

    $.fn[pluginName] = function(options, params) {
      return this.each(function() {
        var instance = $.data(this, pluginName);
        if (!instance) {
          $.data(this, pluginName, new Plugin(this, options));
        } else if (instance[options]) {
          instance[options](params);
        }
      });
    };

    $(function() {
      $('[data-' + pluginName + ']')[pluginName]();
    });
  })(jQuery);
