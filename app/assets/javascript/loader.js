(function() {
	'use strict';
	var body = $('body'),
		loadingBlock = body.find('[data-loader-block]'),
		timer;

	// body.addClass('frozen');
	$(window).on('load', function(){
		// loadingBlock.fadeOut("slow");
		timer = setTimeout(function(){
			// body.removeClass('frozen');
			loadingBlock.addClass('hidden');
		}, 3000);
		loadingBlock.css('opacity','0');
	});
})();


